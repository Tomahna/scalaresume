# ScalaResume
[![build status](https://gitlab.com/tomahna/scalaresume/badges/master/build.svg)](https://gitlab.com/tomahna/scalaresume/commits/master)
[![coverage report](https://gitlab.com/tomahna/scalaresume/badges/master/coverage.svg)](https://gitlab.com/tomahna/scalaresume/commits/master)

## Installation Instruction
### Debian
```
sudo apt-get install curl openjdk-8-jre
curl -s https://packagecloud.io/install/repositories/tomahna/scalaresume/script.deb.sh | sudo bash
sudo apt-get install scalaresume-cli
```
