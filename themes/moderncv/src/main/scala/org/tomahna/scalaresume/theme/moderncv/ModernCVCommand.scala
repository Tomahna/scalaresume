package org.tomahna.scalaresume.theme.moderncv

import java.time.LocalDate
import java.time.format.DateTimeFormatter

import org.tomahna.scalaresume.resume.Education
import org.tomahna.scalaresume.resume.Interest
import org.tomahna.scalaresume.resume.Language
import org.tomahna.scalaresume.resume.Profile
import org.tomahna.scalaresume.resume.Skill
import org.tomahna.scalaresume.resume.Volunteer
import org.tomahna.scalaresume.resume.Work
import org.tomahna.scalaresume.theme.latex.Tex

object ModernCVCommand {
  def moderncvtheme(color: String)(theme: String) = Tex { s"\\moderncvtheme[$color]{$theme}" }

  /**
   * defines one's address
   */
  def address(street: String)(city: String = "")(country: String = ""): Tex = optionCommand(street, city, country) { s"\\address{$street}{$city}{$country}" }

  /**
   * defines one's email
   */
  def email(email: String): Tex = optionCommand(email) { s"\\email{$email}" }

  /**
   * defines one's home page
   */
  def homepage(url: String): Tex = optionCommand(url) { s"\\homepage{$url}" }

  /**
   * adds a mobile number to one's personal information
   */
  def mobile(mobile: String): Tex = optionCommand(mobile) { s"\\mobile{$mobile}" }

  /**
   * defines one's home name
   */
  def name(firstName: String)(lastName: String = ""): Tex = optionCommand(firstName, lastName) { s"\\name{$firstName}{$lastName}" }

  /**
   * defines one's picture
   */
  def picture(width: String)(frameThickness: String)(filename: String): Tex = optionCommand(filename) { s"\\photo[$width][$frameThickness]{$filename}" }

  /**
   * adds a social link to one's personal information
   */
  def social(profile: Profile): Tex = social(profile.network)(profile.url)(profile.username)
  /**
   * adds a social link to one's personal information
   */
  def social(`type`: String)(url: String)(account: String): Tex = Tex {
    println("social")
    s"\\social[${`type`}][$url]{$account}"
  }

  /**
   * defines one's title
   */
  def title(title: String): Tex = optionCommand(title) { s"\\title{$title}" }

  /**
   * makes a resume line with a header and a corresponding text
   */
  def cvitem(skill: Skill): Tex = cvitem(skill.name)(skill.keywords.mkString(", "))
  /**
   * makes a resume line with a header and a corresponding text
   */
  def cvitem(interest: Interest): Tex = cvitem(interest.name)(interest.keywords.mkString(", "))
  /**
   * makes a resume line with a header and a corresponding text
   */
  def cvitem(header: String)(text: String): Tex = Tex {
    s"\\cvitem{$header}{$text}"
  }

  /**
   * makes a typical resume job / education entry
   */
  def cventry(education: Education, dateFormat: String): Tex = {
    val years = period(education.startDate, education.endDate, dateFormat)
    cventry(years)(education.institution)(education.studyType)("")("")(education.courses.mkString(", "))
  }
  /**
   * makes a typical resume job / education entry
   */
  def cventry(volunteer: Volunteer, dateFormat: String): Tex = {
    val years = period(volunteer.startDate, volunteer.endDate, dateFormat)
    cventry(years)(volunteer.organisation)(volunteer.position)("")("")(volunteer.summary)
  }
  /**
   * makes a typical resume job / education entry
   */
  def cventry(work: Work, dateFormat: String): Tex = {
    val years = period(work.startDate, work.endDate, dateFormat)
    cventry(years)(work.company)(work.position)("")("")(work.summary)
  }
  /**
   *  makes a typical resume job / education entry
   */
  def cventry(years: String)(jobTitle: String)(institution: String)(localization: String)(grade: String)(description: String): Tex = Tex {
    s"\\cventry{$years}{$jobTitle}{$institution}{$localization}{$grade}{$description}"
  }

  /**
   *  makes a typical resume language entry
   */
  def cvlanguage(language: Language): Tex = cvlanguage(language.language)(language.fluency)
  /**
   *  makes a typical resume language entry
   */
  def cvlanguage(language: String)(level: String): Tex = Tex {
    s"\\cvlanguage{$language}{$level}{}"
  }

  def optionCommand(value: String)(command: => String): Tex = Tex {
    if (value.isEmpty()) "" else command
  }
  def optionCommand(value: String*)(command: => String): Tex = Tex {
    if (!value.exists(x => !x.isEmpty)) "" else command
  }

  def period(startDate: Option[LocalDate], endDate: Option[LocalDate], format: String): String = {
    val formatter = DateTimeFormatter.ofPattern(format)
    val start = startDate.map(_.format(formatter))
    val end = endDate.map(_.format(formatter)).getOrElse("Aujourd'hui")
    start.map(s => s"$s à $end").getOrElse("")
  }
}
