package org.tomahna.scalaresume.theme.moderncv

import java.io.BufferedWriter
import java.io.File
import java.io.FileWriter
import java.net.URL

import org.apache.commons.io.FileUtils
import org.tomahna.scalaresume.resume.Resume
import org.tomahna.scalaresume.theme.Theme

import resource.managed

class ModernCVTheme extends Theme {
  lazy val name: String = "base"

  override def outputTex(resume: Resume, outputFile: File): Unit = {
    if (!outputFile.exists()) {
      outputFile.getParentFile().mkdirs()
      outputFile.createNewFile()
    }

    val picture = if (resume.basics.picture.isEmpty()) "" else {
      handlePicture(resume.basics.picture, outputFile)
    }

    for (output <- managed(new BufferedWriter(new FileWriter(outputFile)))) {
      output.write(tex.modernCV(resume, picture).body)
    }
  }

  def handlePicture(picture: String, outputFile: File): String = {
    val libDirectory = new File(outputFile.getParentFile(), "libs")
    if (!libDirectory.exists()) {
      libDirectory.mkdirs()
    }

    picture match {
      case _ if picture.startsWith("http") => handleRemotePicture(new URL(picture), libDirectory)
      case _ if new File(picture).exists() => handleLocalPicture(new File(picture), libDirectory)
    }
  }

  def handleLocalPicture(file: File, outputDir: File): String = {
    val pictureFile = new File(outputDir, file.getCanonicalPath())
    FileUtils.copyFile(file, pictureFile)
    pictureFile.getAbsolutePath()
  }

  def handleRemotePicture(url: URL, outputDir: File): String = {
    val pictureFile = new File(outputDir, url.getFile())
    FileUtils.copyURLToFile(url, pictureFile)
    pictureFile.getAbsolutePath()
  }
}