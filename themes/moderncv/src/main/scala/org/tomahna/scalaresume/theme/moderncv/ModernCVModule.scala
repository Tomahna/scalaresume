package org.tomahna.scalaresume.theme.moderncv

import org.tomahna.scalaresume.theme.Theme

import com.google.inject.AbstractModule
import com.google.inject.multibindings.MapBinder

class ModernCVModule extends AbstractModule {
  override def configure(): Unit = {
    val themeBinder = MapBinder.newMapBinder(binder(), classOf[String], classOf[Theme])
    themeBinder.addBinding("moderncv").to(classOf[ModernCVTheme])
    ()
  }
}