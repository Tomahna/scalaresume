package org.tomahna.scalaresume.theme.moderncv

import java.io.File

import scala.io.Source

import org.junit.runner.RunWith
import org.scalatest.FlatSpec
import org.scalatest.Matchers
import org.scalatest.junit.JUnitRunner
import org.tomahna.scalaresume.resume.Resume

import play.api.libs.json.Json
import resource.managed

@RunWith(classOf[JUnitRunner])
class ModernCVThemeTest extends FlatSpec with Matchers {
  val tmpDir = System.getProperty("java.io.tmpdir")

  "Base Theme" should "output a tex file" in {
    for (stream <- managed(getClass.getResourceAsStream("resume.json"))) {
      val content = Source.fromInputStream(stream, "utf-8").mkString
      val resume = Json.parse(content).as[Resume]
      val baseTheme = new ModernCVTheme()
      val testFile = new File(tmpDir, "resume.tex")
      baseTheme.outputTex(resume, testFile)
    }
  }
}