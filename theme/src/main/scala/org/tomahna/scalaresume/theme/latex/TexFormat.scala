package org.tomahna.scalaresume.theme.latex

import scala.collection.immutable

import play.twirl.api.BufferedContent
import play.twirl.api.Format
import play.twirl.api.Formats

/**
 * Content type used in default latex templates.
 */
class Tex private (elements: immutable.Seq[Tex], text: String) extends BufferedContent[Tex](elements, text) {
  def this(text: String) = this(Nil, Formats.safe(text))
  def this(elements: immutable.Seq[Tex]) = this(elements, "")

  /**
   * Content type of text (`application/x-tex`).
   */
  def contentType = "application/x-tex"
}

/**
 * Helper for utilities Tex methods.
 */
object Tex {

  /**
   * Creates a latex fragment with initial content specified.
   */
  def apply(text: String): Tex = {
    new Tex(text)
  }
}

/**
 * Formatter for latex content.
 */
object TexFormat extends Format[Tex] {

  /**
   * Create a latex fragment.
   */
  def raw(text: String) = Tex(text)

  /**
   * No need for a safe (escaped) latex fragment.
   */
  def escape(text: String) = Tex(text)

  /**
   * Generate an empty latex fragment
   */
  val empty: Tex = new Tex("")

  /**
   * Create a latex Fragment that holds other fragments.
   */
  def fill(elements: immutable.Seq[Tex]): Tex = new Tex(elements)
}