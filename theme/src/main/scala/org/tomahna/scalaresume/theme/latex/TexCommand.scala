package org.tomahna.scalaresume.theme.latex

import scala.collection.immutable

object TexCommand {
  def document(content: Tex): Tex = TexFormat.fill {
    immutable.Seq(
      Tex("\\begin{document}"),
      content,
      Tex("\\end{document}")
    )
  }

  def section(name: String)(content: Tex): Tex = if (content.body.trim.isEmpty()) {
    TexFormat.empty
  } else {
    TexFormat.fill {
      immutable.Seq(
        Tex(s"\\section{$name}"),
        content
      )
    }
  }

  def documentclass(options: String)(`class`: String): Tex = Tex {
    s"\\documentclass[$options]{${`class`}}"
  }

  def maketitle(): Tex = Tex { "\\maketitle" }

  def usepackage(options: String)(`package`: String): Tex = Tex {
    s"\\usepackage[$options]{${`package`}}"
  }
}