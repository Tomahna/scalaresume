package org.tomahna.scalaresume.theme

import java.util.Map

import com.google.inject.Provider
import com.typesafe.scalalogging.LazyLogging

import javax.inject.Inject

class ThemeManager @Inject() (themes: Map[String, Provider[Theme]]) extends LazyLogging {
  def getTheme(name: String): Theme = themes.get(name).get
}