package org.tomahna.scalaresume.theme

import java.io.File

import org.tomahna.scalaresume.resume.Resume

trait Theme {
  val name: String

  def outputHTML(resume: Resume, outputFile: File): Unit = throw new IllegalArgumentException("Theme does not implement HTMLOutput")

  def outputTex(resume: Resume, outputFile: File): Unit = throw new IllegalArgumentException("Theme does not implement PDFOutput")
}