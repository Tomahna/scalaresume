import sbt._

object Dependencies {
  val guice = Seq(
    "com.google.inject" % "guice",
    "com.google.inject.extensions" % "guice-multibindings").map(_ % "4.1.0")

  val scalaTest = Seq(
    "org.scalatest" %% "scalatest" % "3.0.0",
    "junit" % "junit" % "4.12").map(_ % "test")
    
  val scopt = "com.github.scopt" %% "scopt" % "3.5.0"
}