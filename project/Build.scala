import com.typesafe.sbt.packager.archetypes.JavaAppPackaging

import Debian.debianSettings
import play.twirl.sbt.Import.TwirlKeys
import play.twirl.sbt.SbtTwirl
import sbt.Build
import sbt.Compile
import sbt.ConfigKey.configurationToKey
import sbt.Def.macroValueI
import sbt.Keys.baseDirectory
import sbt.Keys.console
import sbt.Keys.libraryDependencies
import sbt.Keys.name
import sbt.Keys.organization
import sbt.Keys.scalaVersion
import sbt.Keys.scalacOptions
import sbt.Keys.unmanagedSourceDirectories
import sbt.Keys.version
import sbt.Project
import sbt.classpathDependency
import sbt.file
import sbt.project
import sbt.richFile
import sbt.toGroupID
import sbtbuildinfo.BuildInfoKey
import sbtbuildinfo.BuildInfoKeys.buildInfoKeys
import sbtbuildinfo.BuildInfoKeys.buildInfoPackage
import sbtbuildinfo.BuildInfoPlugin
import sbtrelease.ReleasePlugin.autoImport.ReleaseStep
import sbtrelease.ReleasePlugin.autoImport.ReleaseTransformations.checkSnapshotDependencies
import sbtrelease.ReleasePlugin.autoImport.ReleaseTransformations.commitNextVersion
import sbtrelease.ReleasePlugin.autoImport.ReleaseTransformations.commitReleaseVersion
import sbtrelease.ReleasePlugin.autoImport.ReleaseTransformations.inquireVersions
import sbtrelease.ReleasePlugin.autoImport.ReleaseTransformations.publishArtifacts
import sbtrelease.ReleasePlugin.autoImport.ReleaseTransformations.pushChanges
import sbtrelease.ReleasePlugin.autoImport.ReleaseTransformations.runTest
import sbtrelease.ReleasePlugin.autoImport.ReleaseTransformations.setNextVersion
import sbtrelease.ReleasePlugin.autoImport.ReleaseTransformations.setReleaseVersion
import sbtrelease.ReleasePlugin.autoImport.ReleaseTransformations.tagRelease
import sbtrelease.ReleasePlugin.autoImport.releaseProcess

object ScalaResumeBuild extends Build {
  lazy val commonSettings = Seq(
    organization := "org.tomahna",
    scalaVersion := "2.11.8",
    scalacOptions ++= Seq(
      "-Yrangepos",
      "-Xlint",
      "-deprecation",
      "-feature",
      "-encoding", "UTF-8",
      "-unchecked",
      "-Yno-adapted-args",
      "-Ywarn-dead-code",
      "-Ywarn-numeric-widen",
      "-Ywarn-value-discard",
      "-Xfuture",
      "-Ywarn-unused",
      "-Ywarn-unused-import",
      "-Ydelambdafy:method",
      "-Ybackend:GenBCode",
      "-target:jvm-1.8"),
    scalacOptions in (Compile, console) := Seq())

  def themeProject(name: String, dir: String): Project = {
    Project(name, file(dir))
      .enablePlugins(SbtTwirl)
      .settings(commonSettings: _*)
      .settings(
        libraryDependencies ++= Dependencies.guice,
        libraryDependencies += "com.typesafe.scala-logging" %% "scala-logging" % "3.4.0",
        libraryDependencies ++= Dependencies.scalaTest)
      .settings(
        TwirlKeys.templateImports += "org.tomahna.scalaresume.resume._",
        unmanagedSourceDirectories in Compile += baseDirectory.value / "src/main/twirl")
      .dependsOn(resume)
  }

  lazy val resume = (project in file("resume"))
    .settings(commonSettings: _*)
    .settings(
      name := "scalaresume-resume",
      libraryDependencies += "com.typesafe.play" %% "play-json" % "2.5.6",
      libraryDependencies ++= Dependencies.scalaTest)
  lazy val theme = themeProject("scalaresume-theme", "theme")
  lazy val themeModernCV = themeProject("scalaresume-theme-moderncv", "themes/moderncv")
    .settings(libraryDependencies += "com.jsuereth" %% "scala-arm" % "1.4")
    .settings(libraryDependencies += "commons-io" % "commons-io" % "2.5")
    .settings(
      TwirlKeys.templateImports += "org.tomahna.scalaresume.theme.latex.Tex",
      TwirlKeys.templateFormats += ("tex" -> "org.tomahna.scalaresume.theme.latex.TexFormat"))
    .dependsOn(theme)
  lazy val cli = (project in file("cli"))
    .enablePlugins(BuildInfoPlugin)
    .enablePlugins(JavaAppPackaging)
    .settings(commonSettings: _*)
    .settings(debianSettings: _*)
    .settings(
      name := "scalaresume-cli",
      libraryDependencies ++= Dependencies.guice,
      libraryDependencies += Dependencies.scopt,
      libraryDependencies += "ch.qos.logback" % "logback-classic" % "1.1.7",
      libraryDependencies ++= Dependencies.scalaTest,
      buildInfoKeys := Seq[BuildInfoKey](name, version),
      buildInfoPackage := "org.tomahna.scalaresume.cli")
    .dependsOn(resume)
    .dependsOn(theme)
    .dependsOn(themeModernCV)

  releaseProcess := Seq[ReleaseStep](
    checkSnapshotDependencies, 
    inquireVersions,
    runTest,
    setReleaseVersion,
    commitReleaseVersion,
    tagRelease,
    setNextVersion,
    commitNextVersion,
    pushChanges
    )
}