import com.typesafe.sbt.packager.Keys._

object Debian {
  val debianSettings = Seq(
    maintainer := "Kévin Rauscher <kevin.rauscher@tomahna.fr>",
    packageSummary := "JsonResume Compiler",
    packageDescription := """A small JsonResume compiler written in scala"""
  )
}