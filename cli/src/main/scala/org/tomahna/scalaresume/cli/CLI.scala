package org.tomahna.scalaresume.cli

import java.io.File
import java.util.ServiceLoader

import scala.io.Source

import org.tomahna.scalaresume.resume.Resume
import org.tomahna.scalaresume.theme.ThemeManager

import com.google.inject.Guice
import com.google.inject.Module

import play.api.libs.json.Json

object CLI {
  private val parser = new scopt.OptionParser[Configuration](BuildInfo.name) {
    head(BuildInfo.name, BuildInfo.version)
    help("help").text("prints this usage text")
    arg[File]("inputResume").minOccurs(1).maxOccurs(1)
      .action { (x, c) => c.copy(inputResume = x) }
      .text("input File")
    arg[File]("outputResume").minOccurs(1).maxOccurs(1)
      .action { (x, c) => c.copy(outputResume = x) }
      .text("output File")
    opt[String]('t', "theme")
      .action { (x, c) => c.copy(theme = x) }
      .text("Theme to use for printing")
  }

  def main(args: Array[String]): Unit = {
    parser.parse(args, Configuration()) match {
      case Some(config) => run(config)
      case None =>
    }
  }

  def run(configuration: Configuration): Unit = {
    val injector = Guice.createInjector(ServiceLoader.load(classOf[Module]))
    val themeManager = injector.getInstance(classOf[ThemeManager])
    val theme = themeManager.getTheme(configuration.theme)

    val input = Source.fromFile(configuration.inputResume, "utf-8").mkString
    val resume = Json.parse(input).as[Resume]
    theme.outputTex(resume, configuration.outputResume)
  }
}