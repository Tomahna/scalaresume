package org.tomahna.scalaresume.cli

import java.io.File

case class Configuration(
  inputResume: File = null,
  outputResume: File = null,
  theme: String = "base"
)