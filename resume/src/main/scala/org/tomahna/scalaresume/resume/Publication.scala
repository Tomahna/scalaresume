package org.tomahna.scalaresume.resume

import play.api.libs.json.JsSuccess
import play.api.libs.json.JsValue.jsValueToJsLookup
import play.api.libs.json.Reads
import java.time.LocalDate

case class Publication(
  name: String,
  publisher: String,
  releaseDate: Option[LocalDate],
  website: String,
  summary: String
)

object Publication {
  implicit val publicationReads: Reads[Publication] = Reads {
    json =>
      JsSuccess(
        Publication(
          (json \ "name").as[String],
          (json \ "publisher").as[String],
          (json \ "releaseDate").asOpt[LocalDate],
          (json \ "website").as[String],
          (json \ "summary").as[String]
        )
      )
  }
}