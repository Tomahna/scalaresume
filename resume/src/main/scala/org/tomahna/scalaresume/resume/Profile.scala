package org.tomahna.scalaresume.resume

import play.api.libs.json.Reads
import play.api.libs.json.JsSuccess

case class Profile(
  network: String,
  username: String,
  url: String
)

object Profile {
  implicit val profileReads: Reads[Profile] = Reads {
    json =>
      JsSuccess(
        Profile(
          (json \ "network").as[String],
          (json \ "username").as[String],
          (json \ "url").as[String]
        )
      )
  }
}