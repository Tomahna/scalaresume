package org.tomahna.scalaresume.resume

import play.api.libs.json.Reads
import play.api.libs.json.JsSuccess

case class Skill(
  name: String,
  level: String,
  keywords: List[String]
)

object Skill {
  implicit val skillReads: Reads[Skill] = Reads {
    json =>
      JsSuccess(
        Skill(
          (json \ "name").as[String],
          (json \ "level").as[String],
          (json \ "keywords").as[List[String]]
        )
      )
  }
}