package org.tomahna.scalaresume.resume

import play.api.libs.json.Reads
import play.api.libs.json.JsSuccess
import java.time.LocalDate

case class Award(
  title: String,
  date: Option[LocalDate],
  awarder: String,
  summary: String
)

object Award {
  implicit val awardReads: Reads[Award] = Reads {
    json =>
      JsSuccess(
        Award(
          (json \ "title").as[String],
          (json \ "date").asOpt[LocalDate],
          (json \ "awarder").as[String],
          (json \ "summary").as[String]
        )
      )
  }
}