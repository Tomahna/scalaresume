package org.tomahna.scalaresume.resume

import play.api.libs.json.Reads
import play.api.libs.json.JsSuccess

case class Language(
  language: String,
  fluency: String
)

object Language {
  implicit val languageReads: Reads[Language] = Reads {
    json =>
      JsSuccess(
        Language(
          (json \ "language").as[String],
          (json \ "fluency").as[String]
        )
      )
  }
}