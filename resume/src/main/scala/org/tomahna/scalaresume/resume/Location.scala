package org.tomahna.scalaresume.resume

import play.api.libs.json.JsSuccess
import play.api.libs.json.JsValue.jsValueToJsLookup
import play.api.libs.json.Reads

case class Location(
  address: String,
  postalCode: String,
  city: String,
  countryCode: String,
  region: String
)

object Location {
  implicit val locationReads: Reads[Location] = Reads {
    json =>
      JsSuccess(
        Location(
          (json \ "address").as[String],
          (json \ "postalCode").as[String],
          (json \ "city").as[String],
          (json \ "countryCode").as[String],
          (json \ "region").as[String]
        )
      )
  }
}