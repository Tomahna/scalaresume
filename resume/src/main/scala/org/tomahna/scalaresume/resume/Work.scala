package org.tomahna.scalaresume.resume

import play.api.libs.json.Reads
import play.api.libs.json.JsSuccess
import java.time.LocalDate

case class Work(
  company: String,
  position: String,
  website: String,
  startDate: Option[LocalDate],
  endDate: Option[LocalDate],
  summary: String,
  highlihts: List[String]
)

object Work {
  implicit val workReads: Reads[Work] = Reads {
    json =>
      JsSuccess(
        Work(
          (json \ "company").as[String],
          (json \ "position").as[String],
          (json \ "website").as[String],
          (json \ "startDate").asOpt[LocalDate],
          (json \ "endDate").asOpt[LocalDate],
          (json \ "summary").as[String],
          (json \ "highlights").as[List[String]]
        )
      )
  }
}