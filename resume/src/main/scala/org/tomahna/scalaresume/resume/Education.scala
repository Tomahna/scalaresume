package org.tomahna.scalaresume.resume

import play.api.libs.json.Reads
import play.api.libs.json.JsSuccess
import java.time.LocalDate

case class Education(
  institution: String,
  area: String,
  studyType: String,
  startDate: Option[LocalDate],
  endDate: Option[LocalDate],
  gpa: String,
  courses: List[String]
)

object Education {
  implicit val educationReads: Reads[Education] = Reads {
    json =>
      JsSuccess(
        Education(
          (json \ "institution").as[String],
          (json \ "area").as[String],
          (json \ "studyType").as[String],
          (json \ "startDate").asOpt[LocalDate],
          (json \ "endDate").asOpt[LocalDate],
          (json \ "gpa").as[String],
          (json \ "courses").as[List[String]]
        )
      )
  }
}