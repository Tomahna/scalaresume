package org.tomahna.scalaresume.resume

import play.api.libs.json.JsSuccess
import play.api.libs.json.Reads

case class Resume(
  basics: Basics,
  work: List[Work],
  volunteer: List[Volunteer],
  education: List[Education],
  awards: List[Award],
  publications: List[Publication],
  skills: List[Skill],
  languages: List[Language],
  interests: List[Interest],
  references: List[Reference]
)

object Resume {
  implicit val resumeReads: Reads[Resume] = Reads {
    json =>
      JsSuccess(
        Resume(
          (json \ "basics").as[Basics],
          (json \ "work").as[List[Work]],
          (json \ "volunteer").as[List[Volunteer]],
          (json \ "education").as[List[Education]],
          (json \ "awards").as[List[Award]],
          (json \ "publications").as[List[Publication]],
          (json \ "skills").as[List[Skill]],
          (json \ "languages").as[List[Language]],
          (json \ "interests").as[List[Interest]],
          (json \ "references").as[List[Reference]]
        )
      )
  }
}