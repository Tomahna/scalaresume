package org.tomahna.scalaresume.resume

import play.api.libs.json.Reads
import play.api.libs.json.JsSuccess

case class Reference(
  name: String,
  reference: String
)

object Reference {
  implicit val referenceReads: Reads[Reference] = Reads {
    json =>
      JsSuccess(
        Reference(
          (json \ "name").as[String],
          (json \ "reference").as[String]
        )
      )
  }
}