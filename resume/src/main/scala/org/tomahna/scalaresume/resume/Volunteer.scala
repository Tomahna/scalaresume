package org.tomahna.scalaresume.resume

import java.time.LocalDate

import play.api.libs.json.JsSuccess
import play.api.libs.json.Reads

case class Volunteer(
  organisation: String,
  position: String,
  website: String,
  startDate: Option[LocalDate],
  endDate: Option[LocalDate],
  summary: String,
  highlights: List[String]
)

object Volunteer {
  implicit val volunteerReads: Reads[Volunteer] = Reads {
    json =>
      JsSuccess(
        Volunteer(
          (json \ "organization").as[String],
          (json \ "position").as[String],
          (json \ "website").as[String],
          (json \ "startDate").asOpt[LocalDate],
          (json \ "endDate").asOpt[LocalDate],
          (json \ "summary").as[String],
          (json \ "highlights").as[List[String]]
        )
      )
  }
}