package org.tomahna.scalaresume.resume

import play.api.libs.json.JsSuccess
import play.api.libs.json.JsValue.jsValueToJsLookup
import play.api.libs.json.Reads

case class Basics(
  name: String,
  label: String,
  picture: String,
  email: String,
  phone: String,
  website: String,
  summary: String,
  location: Location,
  profile: List[Profile]
)

object Basics {
  implicit val basicsReads: Reads[Basics] = Reads {
    json =>
      JsSuccess(
        Basics(
          (json \ "name").as[String],
          (json \ "label").as[String],
          (json \ "picture").as[String],
          (json \ "email").as[String],
          (json \ "phone").as[String],
          (json \ "website").as[String],
          (json \ "summary").as[String],
          (json \ "location").as[Location],
          (json \ "profiles").as[List[Profile]]
        )
      )
  }
}