package org.tomahna.scalaresume.resume

import play.api.libs.json.JsSuccess
import play.api.libs.json.JsValue.jsValueToJsLookup
import play.api.libs.json.Reads

case class Interest(
  name: String,
  keywords: List[String]
)

object Interest {
  implicit val interestReads: Reads[Interest] = Reads {
    json =>
      JsSuccess(
        Interest(
          (json \ "name").as[String],
          (json \ "keywords").as[List[String]]
        )
      )
  }
}