package org.tomahna.scalaresume.resume

import scala.io.Source

import org.junit.runner.RunWith
import org.scalatest.FlatSpec
import org.scalatest.Matchers
import org.scalatest.junit.JUnitRunner

import play.api.libs.json.Json

@RunWith(classOf[JUnitRunner])
class LanguageTest extends FlatSpec with Matchers {
  "Language" should "be parsed" in {
    val stream = getClass.getResourceAsStream("language.json")
    val content = Source.fromInputStream(stream, "utf-8").mkString
    stream.close()

    Json.parse(content).as[Language] shouldBe Language(
      "English",
      "Native speaker"
    )
  }
}