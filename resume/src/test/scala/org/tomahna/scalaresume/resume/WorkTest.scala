package org.tomahna.scalaresume.resume

import scala.io.Source

import org.junit.runner.RunWith
import org.scalatest.FlatSpec
import org.scalatest.Matchers
import org.scalatest.junit.JUnitRunner

import play.api.libs.json.Json
import java.time.LocalDate

@RunWith(classOf[JUnitRunner])
class WorkTest extends FlatSpec with Matchers {
  "Work" should "be parsed" in {
    val stream = getClass.getResourceAsStream("work.json")
    val content = Source.fromInputStream(stream, "utf-8").mkString
    stream.close()

    Json.parse(content).as[Work] shouldBe Work(
      "Pied Piper",
      "CEO/President",
      "http://piedpiper.com",
      Some(LocalDate.parse("2013-12-01")),
      Some(LocalDate.parse("2014-12-01")),
      "Pied Piper is a multi-platform technology based on a proprietary universal compression algorithm that has consistently fielded high Weisman Scores™ that are not merely competitive, but approach the theoretical limit of lossless compression.",
      List(
        "Build an algorithm for artist to detect if their music was violating copy right infringement laws",
        "Successfully won Techcrunch Disrupt",
        "Optimized an algorithm that holds the current world record for Weisman Scores"
      )
    )
  }
}