package org.tomahna.scalaresume.resume

import java.time.LocalDate

import scala.io.Source

import org.junit.runner.RunWith
import org.scalatest.FlatSpec
import org.scalatest.Matchers
import org.scalatest.junit.JUnitRunner

import play.api.libs.json.Json

@RunWith(classOf[JUnitRunner])
class EducationTest extends FlatSpec with Matchers {
  "Education" should "be parsed" in {
    val stream = getClass.getResourceAsStream("education.json")
    val content = Source.fromInputStream(stream, "utf-8").mkString
    stream.close()

    Json.parse(content).as[Education] shouldBe Education(
      "University of Oklahoma",
      "Information Technology",
      "Bachelor",
      Some(LocalDate.parse("2011-06-01")),
      Some(LocalDate.parse("2014-01-01")),
      "4.0",
      List(
        "DB1101 - Basic SQL",
        "CS2011 - Java Introduction"
      )
    )
  }
}