package org.tomahna.scalaresume.resume

import scala.io.Source

import org.junit.runner.RunWith
import org.scalatest.FlatSpec
import org.scalatest.Matchers
import org.scalatest.junit.JUnitRunner

import play.api.libs.json.Json
import java.time.LocalDate

@RunWith(classOf[JUnitRunner])
class AwardTest extends FlatSpec with Matchers {
  "Award" should "be parsed" in {
    val stream = getClass.getResourceAsStream("award.json")
    val content = Source.fromInputStream(stream, "utf-8").mkString
    stream.close()

    Json.parse(content).as[Award] shouldBe Award(
      "Digital Compression Pioneer Award",
      Some(LocalDate.parse("2014-11-01")),
      "Techcrunch",
      "There is no spoon."
    )
  }
}