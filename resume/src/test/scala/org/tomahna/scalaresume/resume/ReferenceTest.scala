package org.tomahna.scalaresume.resume

import scala.io.Source

import org.junit.runner.RunWith
import org.scalatest.FlatSpec
import org.scalatest.Matchers
import org.scalatest.junit.JUnitRunner

import play.api.libs.json.Json

@RunWith(classOf[JUnitRunner])
class ReferenceTest extends FlatSpec with Matchers {
  "Reference" should "be parsed" in {
    val stream = getClass.getResourceAsStream("reference.json")
    val content = Source.fromInputStream(stream, "utf-8").mkString
    stream.close()

    Json.parse(content).as[Reference] shouldBe Reference(
      "Erlich Bachman",
      "It is my pleasure to recommend Richard, his performance working as a consultant for Main St. Company proved that he will be a valuable addition to any company."
    )
  }
}