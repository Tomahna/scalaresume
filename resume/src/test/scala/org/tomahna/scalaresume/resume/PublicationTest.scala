package org.tomahna.scalaresume.resume

import scala.io.Source

import org.junit.runner.RunWith
import org.scalatest.FlatSpec
import org.scalatest.Matchers
import org.scalatest.junit.JUnitRunner

import play.api.libs.json.Json
import java.time.LocalDate

@RunWith(classOf[JUnitRunner])
class PublicationTest extends FlatSpec with Matchers {
  "Publication" should "be parsed" in {
    val stream = getClass.getResourceAsStream("publication.json")
    val content = Source.fromInputStream(stream, "utf-8").mkString
    stream.close()

    Json.parse(content).as[Publication] shouldBe Publication(
      "Video compression for 3d media",
      "Hooli",
      Some(LocalDate.parse("2014-10-01")),
      "http://en.wikipedia.org/wiki/Silicon_Valley_(TV_series)",
      "Innovative middle-out compression algorithm that changes the way we store data."
    )
  }
}