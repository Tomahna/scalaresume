package org.tomahna.scalaresume.resume

import scala.io.Source

import org.junit.runner.RunWith
import org.scalatest.FlatSpec
import org.scalatest.Matchers
import org.scalatest.junit.JUnitRunner
import play.api.libs.json.Json

@RunWith(classOf[JUnitRunner])
class BasicsTest extends FlatSpec with Matchers {
  "Basics" should "be parsed" in {
    val stream = getClass.getResourceAsStream("basics.json")
    val content = Source.fromInputStream(stream, "utf-8").mkString
    stream.close()

    Json.parse(content).as[Basics] shouldBe Basics(
      "Richard Hendriks",
      "Programmer",
      "",
      "richard.hendriks@mail.com",
      "(912) 555-4321",
      "http://richardhendricks.com",
      "Richard hails from Tulsa. He has earned degrees from the University of Oklahoma and Stanford. (Go Sooners and Cardinals!) Before starting Pied Piper, he worked for Hooli as a part time software developer. While his work focuses on applied information theory, mostly optimizing lossless compression schema of both the length-limited and adaptive variants, his non-work interests range widely, everything from quantum computing to chaos theory. He could tell you about it, but THAT would NOT be a “length-limited” conversation!",
      Location(
        "2712 Broadway St",
        "CA 94115",
        "San Francisco",
        "US",
        "California"
      ),
      List(
        Profile(
          "Twitter",
          "neutralthoughts",
          ""
        ),
        Profile(
          "SoundCloud",
          "dandymusicnl",
          "https://soundcloud.com/dandymusicnl"
        )
      )
    )
  }
}