package org.tomahna.scalaresume.resume

import scala.io.Source

import org.junit.runner.RunWith
import org.scalatest.FlatSpec
import org.scalatest.Matchers
import org.scalatest.junit.JUnitRunner

import play.api.libs.json.Json

@RunWith(classOf[JUnitRunner])
class InterestTest extends FlatSpec with Matchers {
  "Interest" should "be parsed" in {
    val stream = getClass.getResourceAsStream("interest.json")
    val content = Source.fromInputStream(stream, "utf-8").mkString
    stream.close()

    Json.parse(content).as[Interest] shouldBe Interest(
      "Wildlife",
      List(
        "Ferrets",
        "Unicorns"
      )
    )
  }
}