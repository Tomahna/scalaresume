package org.tomahna.scalaresume.resume

import scala.io.Source

import org.junit.runner.RunWith
import org.scalatest.FlatSpec
import org.scalatest.Matchers
import org.scalatest.junit.JUnitRunner
import java.time.LocalDate
import play.api.libs.json.Json

@RunWith(classOf[JUnitRunner])
class ResumeTest extends FlatSpec with Matchers {
  "Resume" should "be parsed" in {
    val stream = getClass.getResourceAsStream("resume.json")
    val content = Source.fromInputStream(stream, "utf-8").mkString
    stream.close()

    Json.parse(content).as[Resume] shouldBe Resume(
      Basics(
        "Richard Hendriks",
        "Programmer",
        "",
        "richard.hendriks@mail.com",
        "(912) 555-4321",
        "http://richardhendricks.com",
        "Richard hails from Tulsa. He has earned degrees from the University of Oklahoma and Stanford. (Go Sooners and Cardinals!) Before starting Pied Piper, he worked for Hooli as a part time software developer. While his work focuses on applied information theory, mostly optimizing lossless compression schema of both the length-limited and adaptive variants, his non-work interests range widely, everything from quantum computing to chaos theory. He could tell you about it, but THAT would NOT be a “length-limited” conversation!",
        Location(
          "2712 Broadway St",
          "CA 94115",
          "San Francisco",
          "US",
          "California"
        ),
        List(
          Profile(
            "Twitter",
            "neutralthoughts",
            ""
          ),
          Profile(
            "SoundCloud",
            "dandymusicnl",
            "https://soundcloud.com/dandymusicnl"
          )
        )
      ),
      List(
        Work(
          "Pied Piper",
          "CEO/President",
          "http://piedpiper.com",
          Some(LocalDate.parse("2013-12-01")),
          Some(LocalDate.parse("2014-12-01")),
          "Pied Piper is a multi-platform technology based on a proprietary universal compression algorithm that has consistently fielded high Weisman Scores™ that are not merely competitive, but approach the theoretical limit of lossless compression.",
          List(
            "Build an algorithm for artist to detect if their music was violating copy right infringement laws",
            "Successfully won Techcrunch Disrupt",
            "Optimized an algorithm that holds the current world record for Weisman Scores"
          )
        )
      ),
      List(
        Volunteer(
          "CoderDojo",
          "Teacher",
          "http://coderdojo.com/",
          Some(LocalDate.parse("2012-01-01")),
          Some(LocalDate.parse("2013-01-01")),
          "Global movement of free coding clubs for young people.",
          List("Awarded 'Teacher of the Month'")
        )
      ),
      List(
        Education(
          "University of Oklahoma",
          "Information Technology",
          "Bachelor",
          Some(LocalDate.parse("2011-06-01")),
          Some(LocalDate.parse("2014-01-01")),
          "4.0",
          List(
            "DB1101 - Basic SQL",
            "CS2011 - Java Introduction"
          )
        )
      ),
      List(
        Award(
          "Digital Compression Pioneer Award",
          Some(LocalDate.parse("2014-11-01")),
          "Techcrunch",
          "There is no spoon."
        )
      ),
      List(
        Publication(
          "Video compression for 3d media",
          "Hooli",
          Some(LocalDate.parse("2014-10-01")),
          "http://en.wikipedia.org/wiki/Silicon_Valley_(TV_series)",
          "Innovative middle-out compression algorithm that changes the way we store data."
        )
      ),
      List(
        Skill(
          "Web Development",
          "Master",
          List(
            "HTML",
            "CSS",
            "Javascript"
          )
        ),
        Skill(
          "Compression",
          "Master",
          List(
            "Mpeg",
            "MP4",
            "GIF"
          )
        )
      ),
      List(
        Language(
          "English",
          "Native speaker"
        )
      ),
      List(
        Interest(
          "Wildlife",
          List(
            "Ferrets",
            "Unicorns"
          )
        )
      ),
      List(
        Reference(
          "Erlich Bachman",
          "It is my pleasure to recommend Richard, his performance working as a consultant for Main St. Company proved that he will be a valuable addition to any company."
        )
      )
    )
  }
}