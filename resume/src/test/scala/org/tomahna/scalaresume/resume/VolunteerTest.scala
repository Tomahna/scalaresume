package org.tomahna.scalaresume.resume

import scala.io.Source

import org.junit.runner.RunWith
import org.scalatest.FlatSpec
import org.scalatest.Matchers
import org.scalatest.junit.JUnitRunner

import play.api.libs.json.Json
import java.time.LocalDate

@RunWith(classOf[JUnitRunner])
class VolunteerTest extends FlatSpec with Matchers {
  "Volunteer" should "be parsed" in {
    val stream = getClass.getResourceAsStream("volunteer.json")
    val content = Source.fromInputStream(stream, "utf-8").mkString
    stream.close()

    Json.parse(content).as[Volunteer] shouldBe Volunteer(
      "CoderDojo",
      "Teacher",
      "http://coderdojo.com/",
      Some(LocalDate.parse("2012-01-01")),
      Some(LocalDate.parse("2013-01-01")),
      "Global movement of free coding clubs for young people.",
      List("Awarded 'Teacher of the Month'")
    )
  }
}